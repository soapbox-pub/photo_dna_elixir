# PhotoDNA Elixir

Unofficial [PhotoDNA](https://www.microsoft.com/en-us/photodna) client for Elixir.

```elixir
# API connection details
conn = %PhotoDNA.Client.Conn{
  base_url: "https://api.microsoftmoderator.com",
  subscription_key: "123456789"
}

# Check a URL
case PhotoDNA.match(conn, "https://example.tld/1.jpg") do
  {:ok, _data} ->
    IO.puts("Everything is fine.")

  {:alert, _data} ->
    IO.puts("CODE RED")
end
```

## Installation

The package can be installed by adding `photo_dna` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:photo_dna, "~> 0.2.0"}
  ]
end
```

# License

photo_dna_elixir is licensed under the MIT license.
See LICENSE.md for the full text.
