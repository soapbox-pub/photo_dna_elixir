defmodule PhotoDNATest do
  use ExUnit.Case, async: true

  @match_url "https://example.tld/csam.jpg"
  @innocuous_url "https://example.tld/flower.jpg"

  @conn %PhotoDNA.Client.Conn{
    base_url: "/",
    subscription_key: "test-key"
  }

  setup do
    Tesla.Mock.mock(fn
      %{method: :post, url: "/photodna/v1.0/Match"} = env ->
        headers = Map.new(env.headers)

        if Map.get(headers, "Ocp-Apim-Subscription-Key") == "test-key" do
          case Jason.decode!(env.body) do
            %{"DataRepresentation" => "URL", "Value" => @match_url} ->
              fixture("test/fixtures/match_3000.json")

            %{"DataRepresentation" => "URL", "Value" => @innocuous_url} ->
              fixture("test/fixtures/match_3000_nonmatch.json")
          end
        else
          fixture("test/fixtures/401.json", status: 401)
        end
    end)
  end

  test "match/2 without a subscription key returns a 401" do
    conn = %PhotoDNA.Client.Conn{
      base_url: "/",
      subscription_key: nil
    }

    {:error, data} = PhotoDNA.match(conn, @innocuous_url)

    assert data["statusCode"] == 401
  end

  test "match/2 with matching image returns a match" do
    {:alert, data} = PhotoDNA.match(@conn, @match_url)

    assert data["Status"]["Code"] == 3000
    assert data["IsMatch"] == true
  end

  test "match/2 with non-matching image returns a non-match" do
    {:ok, data} = PhotoDNA.match(@conn, @innocuous_url)

    assert data["Status"]["Code"] == 3000
    assert data["IsMatch"] == false
  end

  defp fixture(filename, response_opts \\ []) do
    filename
    |> File.read!()
    |> Jason.decode!()
    |> Tesla.Mock.json(response_opts)
  end
end
