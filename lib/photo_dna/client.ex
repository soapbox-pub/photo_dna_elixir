defmodule PhotoDNA.Client do
  @moduledoc """
  HTTP Client for making requests to the PhotoDNA server.
  """
  alias PhotoDNA.Client.Conn

  @doc """
  Construct a new `t:Tesla.Client.t/0`
  """
  @spec new(Conn.t()) :: Tesla.Client.t()
  def new(%Conn{} = conn) do
    middleware = [
      {Tesla.Middleware.BaseUrl, conn.base_url},
      {Tesla.Middleware.Headers,
       [
         {"Ocp-Apim-Subscription-Key", conn.subscription_key},
         {"Enable-Evaluation", conn.enable_evaluation}
       ]},
      Tesla.Middleware.JSON
    ]

    Tesla.client(middleware)
  end

  @doc """
  Perform an HTTP request using connection details from a
  `t:PhotoDNA.Client.Conn.t/0` struct.
  """
  @spec request(Conn.t(), [Tesla.option()]) :: Tesla.Env.result()
  def request(%Conn{} = conn, options) do
    conn
    |> new()
    |> Tesla.request(options)
  end
end
