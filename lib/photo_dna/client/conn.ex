defmodule PhotoDNA.Client.Conn do
  @moduledoc """
  Represents a connection to the PhotoDNA API.
  """
  defstruct base_url: nil, subscription_key: nil, enable_evaluation: nil

  @typedoc """
  Struct representing a connection to the PhotoDNA API.
  """
  @type t :: %__MODULE__{
          base_url: String.t() | nil,
          subscription_key: String.t() | nil,
          enable_evaluation: :always | :afterinitialquota | nil
        }
end
