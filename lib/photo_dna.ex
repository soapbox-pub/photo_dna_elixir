defmodule PhotoDNA do
  @moduledoc """
  Unofficial PhotoDNA client for Elixir.

  - https://www.microsoft.com/en-us/photodna
  - https://developer.microsoftmoderator.com/docs/services/57c7426e2703740ec4c9f4c3
  """
  alias PhotoDNA.Client
  alias PhotoDNA.Client.Conn

  @doc """
  Check a URL against PhotoDNA.

  It can return the following tuples:

  - `{:alert, data}` - CSAM detected
  - `{:ok, data}` - Image is OK
  - `{:error, data}` - API or client error

  ```elixir
  # API connection details
  conn = %PhotoDNA.Client.Conn{
    base_url: "https://api.microsoftmoderator.com",
    subscription_key: "123456789"
  }

  # Check a URL
  case PhotoDNA.match(conn, "https://example.tld/1.jpg") do
    {:ok, _data} ->
      IO.puts("Everything is fine.")

    {:alert, _data} ->
      IO.puts("CODE RED")
  end
  ```
  """
  @spec match(Conn.t(), url :: String.t(), opts :: Keyword.t()) ::
          {:ok, data :: map()} | {:alert, data :: map()} | {:error, data :: map()}
  def match(%Conn{} = conn, url, opts \\ []) do
    params = %{
      "DataRepresentation" => "URL",
      "Value" => url
    }

    request = [
      method: :post,
      url: "/photodna/v1.0/Match",
      body: params,
      query: opts
    ]

    conn
    |> Client.request(request)
    |> parse_match()
  end

  defp parse_match(response) do
    case response do
      # CSAM detected
      {:ok, %{body: %{"IsMatch" => true} = body}} ->
        {:alert, body}

      # Image is fine
      {:ok, %{body: %{"Status" => %{"Code" => 3000}} = body}} ->
        {:ok, body}

      # API error
      {:ok, %{body: body}} ->
        {:error, body}

      # Client error
      {:error, error} ->
        {:error, error}

      # This should never happen
      error ->
        {:error, error}
    end
  end
end
