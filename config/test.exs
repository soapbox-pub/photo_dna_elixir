import Config

# Configure Tesla mock adapter
# https://hexdocs.pm/tesla/Tesla.Mock.html
config :tesla, adapter: Tesla.Mock
